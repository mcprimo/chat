from django.conf.urls import patterns, include, url
from django.contrib import admin
import settings
admin.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'projekt.views.home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
    {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
    url(r'^wyslij/', 'projekt.views.wyslij'),
    url(r'^odbierz/', 'projekt.views.odbierz'),
    url(r'^logowanie/', 'projekt.views.logowanie'),
    url(r'^rejestracja/', 'projekt.views.rejestracja'),
    url(r'^auth/', 'projekt.views.l_auth'),
    url(r'^wyloguj/', 'projekt.views.wyloguj'),

    # url(r'^projekt/', include('projekt.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
