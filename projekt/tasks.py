from celery.decorators import task
import requests
import json
from lxml import objectify
import couchdb
import random

@task()
def rejestracja():
    couch = couchdb.Server('http://194.29.175.241:5984')
    db = couch["chat"]

    for i in db:
        try:
            doc=db[i]
            if doc["host"]=="http://mcprimotest.cloudapp.net/":
                doc["active"]="true"
                db[i]=doc
                return "suc"
        except:
            pass

    db.save({"host":"http://mcprimotest.cloudapp.net/","active":"true","delivery":"odbierz/"})
    return "suc"


@task()
def operacje(op):
    couch = couchdb.Server('http://194.29.175.241:5984')
    db = couch["calc"]
    lista=[]
    lista.append(op)

    for i in db.view("utils/list_active"):
        operator=i.key
        if not (operator in lista):
            lista.append(operator)

    text="Dostepne opreacje to "

    for i in lista:
        text=text+i+" "

    return text


@task()
def wyslij(user,message,timestamp):
    couch = couchdb.Server('http://194.29.175.241:5984')
    db = couch["calc"]
    view=db.view("utils/list_active")

    for i in view:
        try:
            doc=db[i.id]
            host=doc["host"]
            if not str(host)=="http://mcprimotest.cloudapp.net/":
                url=host+doc["delivery"]
                data={"user":user,"message":message,"timestamp":str(timestamp)}
                headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
                r=requests.post(url,json.dumps(data),headers=headers)
        except:
            pass