from models import Wiad
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.context_processors import  csrf
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
import datetime
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse
from forms import Formularz
from django.contrib.auth.models import User
import requests
import json
import tasks
import simplejson as sjson

def home(request):
    tasks.rejestracja.delay()
    form=Formularz()

    return render_to_response("index.html",
    {'wpisy' : Wiad.objects.all().order_by("-id"),"form":form},
    context_instance=RequestContext(request),)

def wyslij(request):
    if request.method == 'POST': # If the form has been submitted...
        wiad=Wiad()
        if request.user.is_authenticated():
            form = Formularz(request.POST) # A form bound to the POST data
            if form.is_valid(): # All validation rules pass
                message=form.cleaned_data["message"]
                user=request.user.username
                timestamp=datetime.datetime.now()

                tasks.wyslij.delay(user,message,timestamp)

                wiad.message=message
                wiad.user=user+"@local"
                wiad.timestamp=timestamp
                wiad.save()

    return HttpResponseRedirect(reverse('projekt.views.home'))

def odbierz(request):
    if request.method == 'POST': # If the form has been submitted...
        wiad=Wiad()
        data = sjson.loads(request.body)
        message=data["message"]
        user=data["user"]
        timestamp=data["timestamp"]

        wiad.message=str(message)
        wiad.user=str(user+"@"+get_client_ip(request))
        wiad.timestamp=datetime.datetime(timestamp)
        wiad.save()

        return HttpResponse(" ")


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def logowanie(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('logowanie.html', c,
                                context_instance=RequestContext(request),)

def l_auth(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username = username, password=password)

    if user is not None:
        if user.is_active:
            auth.login(request, user)
            return HttpResponseRedirect(reverse('projekt.views.home'))
    else:
        return HttpResponseRedirect(reverse('projekt.views.logowanie'))



def wyloguj(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('projekt.views.home'))


def rejestracja(request):
    form = UserCreationForm(request.POST)
    args= {}
    args.update(csrf(request))
    args['form'] = UserCreationForm()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('projekt.views.logowanie'))
        else:
            args['wiad'] = "Wypelnij poprawnie wszystkie pola lub takie konto juz istnieje"
    print args
    return render_to_response('rejestracja.html', args,
                            context_instance=RequestContext(request),)