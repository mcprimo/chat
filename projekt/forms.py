from django.forms import ModelForm
from django import forms
from models import Wiad

class Formularz(forms.ModelForm):
     class Meta:
         model = Wiad
         widgets = {
            'message': forms.TextInput,
         }
         fields = ['message']